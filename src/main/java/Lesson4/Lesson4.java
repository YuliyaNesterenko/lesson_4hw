package Lesson4;

import java.util.Scanner;

public class Lesson4 {
    public static void main(String[] args) {
        int[] iSeries;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите уровень сложности (1 или 2):");
        int iLv = scan.nextInt();
        if ((iLv == 1) || (iLv == 2)) {
            do {
                iSeries = createSeries(iLv);
            } while (checkSeries(iSeries));

            System.out.println("Продолжите ряд:");
            printSeries(iSeries);
            nextNumber(iSeries);
        } else {
            System.out.println("Такого уровня нет.");
        }
    }

    public static int[] createSeries(int iLv) {

        int iArr[] = new int[7];
        iArr[0] = (int) (Math.random() * 50 + 1);
        int iStep1, iStep2;
        do {
            iStep1 = (int) (Math.random() * 11 - 5);
        } while (iStep1 == 0);
        for (int i = 1; i < 7; i++) {
            iArr[i] = iArr[i - 1] + iStep1;
        }

        if (iLv == 2) {
            do {
                iStep2 = (int) (Math.random() * 11 - 5);
            } while (iStep2 == 0);
            iArr[1] = (int) (Math.random() * 50 + 1);
            for (int i = 3; i < 7; i += 2) {
                iArr[i] = iArr[i - 2] + iStep1;
            }
        }
        return iArr;
    }

    public static boolean checkSeries(int[] iArr) {

        boolean flag = false;
        for (int i = 0; i < 7; i++) {
            if ((iArr[i] < 1) || (iArr[i] > 99)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static void printSeries(int[] iArr) {

        for (int i = 0; i < 7; i++) {
            if (i < 6) {
                System.out.print(iArr[i]);
                System.out.print(" ");
            } else {
                System.out.println("..");
            }
        }
    }

    public static void nextNumber(int[] iArr) {

        int i = 1;
        int num;
        Scanner scan = new Scanner(System.in);
        do {
            System.out.println("Введите число");
            num = scan.nextInt();
            if (num == iArr[6]) {
                System.out.println("Верно!");
            } else if (i < 3) {
                System.out.println("Попробуйте ещё");
            } else {
                System.out.println("Это была последняя попытка.");
            }
            i++;
        }
        while ((i < 4) && (num != iArr[6]));
        if (num != iArr[6]) {
            System.out.println("Правильный ответ:");
            System.out.println(iArr[6]);
        } else {
            System.out.println("Молодец");
        }
    }

}
